package com.example.serviceexampleaudio;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
	 Button buttonStart, buttonStop,buttonNext;

	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  setContentView(R.layout.activity_main);

	  buttonStart = (Button) findViewById(R.id.buttonStart);
	  buttonStop = (Button) findViewById(R.id.buttonStop);
	  buttonNext = (Button) findViewById(R.id.buttonNext);


         buttonStart.setOnClickListener(new OnClickListener() {
             @Override
             public void onClick(View view) {
                 Intent i=new Intent(MainActivity.this, MyService.class);
                 startService(i);
             }
         });

         buttonStop.setOnClickListener(new OnClickListener() {
             @Override
             public void onClick(View view) {
                 Intent i=new Intent(MainActivity.this, MyService.class);
                 stopService(i);
             }
         });

         buttonNext.setOnClickListener(new OnClickListener() {
             @Override
             public void onClick(View view) {
                 Intent i=new Intent(MainActivity.this, NextPage.class);
                 startActivity(i);
             }
         });






	 }



}
